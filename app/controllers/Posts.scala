package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import anorm._
import models._
import java.util.{Date}
import views._
import play.api.db._
import play.api.Play.current

object Posts extends Controller {
  
  val postForm: Form[Post] =  Form(
      mapping(
          "id"          -> ignored(NotAssigned: Pk[Long]),
          "title"       -> nonEmptyText,
          "description" -> nonEmptyText,
          "body"        -> nonEmptyText,
          "created_at"  -> optional(date("MM/dd/yy")),
          "updated_at"  -> optional(date("MM/dd/yy")),
          "user_id"     -> longNumber

        )(Post.apply)(Post.unapply)
    )
  

  def form = Action {
    Ok(html.posts.form( postForm ))
  }

  def createPost = Action { implicit request => 
    postForm.bindFromRequest.fold(
      formwithErrors => BadRequest(views.html.posts.form(formwithErrors)),  
        value =>
          DB.withConnection { implicit connection =>
            val now = new java.util.Date()
            val id: Option[Long] = SQL("""insert into Post (title, description, body, created_at, updated_at, user_id) values ({title},
            {description}, {body}, {created_at}, {updated_at}, {user_id})""").on(
                'title -> value.title,
                'description -> value.description,
                'body -> value.body,
                'created_at -> new Date(),
                'updated_at -> None,
                'user_id -> 1
              ).executeInsert()
            Ok(html.posts.test())
 
          }
        ) 
  }
}

