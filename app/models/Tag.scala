package models
import anorm._
import anorm.SqlParser._


case class Tag (
    id: Pk[Long],
    name: String
  )

object Tag {
  val simple = {
    get[Pk[Long]]("tag.id") ~
    get[String]("tag.name") map {
      case id~name => Tag(id, name)
    }
  }
}
