package models
import anorm._
import anorm.SqlParser._

case class User (
  id: Pk[Long],
  email: Option[String],
  password: Option[String],
  username: Option[String],
  isAdmin: Option[Boolean]
    
  )
