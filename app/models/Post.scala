package models
import java.util.{Date}
import anorm._
import anorm.SqlParser._
import models.Tag._

case class Post(
  id          : Pk[Long],
  title       : String,
  description : String,
  body        : String,
  created_at  : Option[Date],
  updated_at  : Option[Date],
  user_id     : Long
)
 
object Post {
  
  // Parsers
  //
  val simple = {
   get[Pk[Long]]("post.id") ~
   get[String]("post.title") ~
   get[String]("post.description") ~
   get[String]("post.body") ~
   get[Option[Date]]("post.created_at") ~
   get[Option[Date]]("post.updated_at") ~
   get[Long]("post.user_id")  map {
      case id~title~description~body~createdAt~updatedAt~userId => Post(id, title, description, body, createdAt, updatedAt,
        userId)
   }
  }
}

