# INDEX TAG

# --- !Ups

ALTER TABLE Tag ADD UNIQUE INDEX name_idx(name);

# --- !Downs

DROP INDEX name_idx on Tag;
