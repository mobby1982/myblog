# INDICES

# --- !Ups

ALTER TABLE PostTag ADD INDEX post_idx(post_id);

# --- !Downs

DROP INDEX post_idx ON PostTag;
