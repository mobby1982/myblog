# INDICES

# --- !Ups

ALTER TABLE PostTag ADD INDEX tag_idx(tag_id);

# --- !Downs

DROP INDEX tag_idx ON PostTag;
