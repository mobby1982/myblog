# USERS SCHEMA

# --- !Ups

CREATE TABLE User (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  email    varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  username varchar(255) NOT NULL,
  isAdmin  boolean NOT NULL, 
  PRIMARY KEY (id)

) CHARACTER SET=utf8;

# --- !Downs

DROP TABLE User;

