# POSTSTAGS SCHEMA 

# -- !Ups

CREATE TABLE PostTag (
  post_id bigint,
  tag_id bigint,
  FOREIGN KEY (post_id) references Post(id),
  FOREIGN KEY (tag_id) references  Tag(id)
) CHARACTER SET=utf8;

# --- !Downs

DROP TABLE PostTag;
