# INDEX USER

# ---!Ups

ALTER TABLE User ADD UNIQUE INDEX email_idx(email);

# --- !Downs

DROP INDEX User email_idx on User;
