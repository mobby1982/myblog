# TAGS SCHEMA

# --- !Ups

CREATE TABLE Tag (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) CHARACTER SET=utf8;


# --- !Downs
DROP TABLE Tag;
