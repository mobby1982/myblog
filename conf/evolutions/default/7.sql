# INDICES POST

# -- !Ups

ALTER TABLE Post ADD INDEX user_id_idx(user_id);


# --- !Downs

DROP INDEX user_id_idx on Post;


