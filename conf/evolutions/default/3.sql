# POST SCHEMA

# --- !Ups

CREATE TABLE Post (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  title text NOT NULL,
  description varchar(255) NOT NULL,
  body text NOT NULL,
  created_at DATE,
  updated_at DATE,
  user_id bigint,
  FOREIGN KEY (user_id) references User(id),
  PRIMARY KEY (id)

) CHARACTER SET=utf8;

# --- !Downs

DROP TABLE Post;

